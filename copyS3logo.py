#!/usr/bin/env python
from multiprocessing.pool import ThreadPool
from time import time as timer
import commands
import os
from tqdm import tqdm

destBucket = "s3://cdn-xm-fileservice/entities/"
imgDir = "imgs"
cmd = []

for filename in os.listdir(imgDir):
    command = "aws s3 cp "+imgDir+"/"+filename+" "+destBucket+filename+"/StoreLogo/v1/"+filename+"_StoreLogo"
    cmd.append(command)

def fetch_url(c):
    status_code, err_msg = commands.getstatusoutput(c)
    if status_code != 0:
        print('Error: '+err_msg)
    
start = timer()
res = list(tqdm(ThreadPool(2).imap_unordered(fetch_url, cmd), total=(len(cmd))))
for i in res:
    pass
print("Elapsed Time: %s" % (timer() - start))
