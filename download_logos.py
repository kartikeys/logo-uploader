#!/usr/bin/env python
from multiprocessing.pool import ThreadPool
from time import time as timer
from urllib import urlopen
import csv

grabPayid_and_urls = []
grabidGrabPayidMap = {}

reader = csv.reader(open('grab_id_pay_mapping.csv'))
for line in reader:
    grabPayid = line[0]
    grabid = line[1]
    grabidGrabPayidMap[grabPayid] = grabid

with open('logo_file.txt', 'r') as logoFile, open('grabpayids.txt', 'r') as gpids:
    for i, line in enumerate(logoFile):
        grabPayid_and_urls.append([])
        grabPayid_and_urls[i].append(line.replace('\n', ''))
    for i, line in enumerate(gpids):
        grabPayid_and_urls[i].append(line.replace('\n', ''))

def fetch_url(grabpayid_url):
    try:
        grabPayid = grabpayid_url[1]
        url = grabpayid_url[0]
        response = urlopen(url)
        filename = grabidGrabPayidMap[grabPayid]
        with open("imgs/"+filename, 'wb') as f:
            f.write(response.read())
        return grabPayid, None
    except Exception as e:
        return grabPayid, e

start = timer()
results = ThreadPool(20).imap_unordered(fetch_url, grabPayid_and_urls)
for grabid_url, error in results:
    if error is not None:
        print("error fetching %r: %s" % (grabid_url, error))

print("Elapsed Time: %s" % (timer() - start))

